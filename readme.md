CSS
---
- [cssnext](http://cssnext.io/features/)

Testing Utils
-------------

- use [expect from chai](http://chaijs.com/api/bdd/)
```
import { expect } from 'chai' 
// ...
    expect(foo).to.not.equal('bar');
    expect(foo).to.deep.equal({ bar: 'baz' });
    expect(foo).to.have.any.keys('bar', 'baz');
    expect({ foo: 'baz' }).to.have.property('foo')
        .and.not.equal('bar');

    expect({ foo: { bar: { baz: 'quux' } } }).to.have.deep.property('foo.bar.baz', 'quux');
```

- assert, manipulate, and traverse your React Components' output: 
  [Enzyme](http://airbnb.io/enzyme/docs/api/index.html)
```
import { mount } from 'enzyme';
// ...
it('renders children when passed in', () => {
    const wrapper = shallow(
      <MyComponent>
        <div className="unique" />
      </MyComponent>
    );
    expect(wrapper.contains(<div className="unique" />)).to.equal(true);
});
```
                                                      
- test function calls with [spies from Sinon](http://sinonjs.org/docs/#spies)
```
import sinon from 'sinon'
//...
it('should call method once with each argument', () => {
    var object = { method: function () {} };
    var spy = sinon.spy(object, "method");
    spy.withArgs(42);
    spy.withArgs(1);

    object.method(42);
    object.method(1);

    assert(spy.withArgs(42).calledOnce);
    assert(spy.withArgs(1).calledOnce);
})
```