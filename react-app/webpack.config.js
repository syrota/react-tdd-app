/* eslint-env node */
const { resolve } = require('path');
const webpack = require('webpack');
const isProduction = process.env.NODE_ENV === 'production';

const config = {

  context: resolve(__dirname, 'src'),

  entry: [
    './index.js'
    // the entry point of our app
  ],

  output: {
    library: 'app',
    libraryTarget: 'var',

    filename: 'bundle.js',
    // the output bundle

    path: resolve(__dirname, 'dist'),

    publicPath: '/'
    // necessary for HMR to know where to load the hot update chunks
  },

  resolve: {
    modules: [resolve(__dirname, 'src'), 'node_modules']
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          'babel-loader',
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader?modules&importLoaders=1',
          'postcss-loader',
        ],
      },
    ],
  },

  plugins: [
  ],

};

if (isProduction) {
  // PRODUCTION

  config.devtool = 'source-map';

  config.plugins.push(

    new webpack.DefinePlugin({
      'process.env': { 'NODE_ENV': JSON.stringify('production') }
    }),

    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true
      },
      compress: {
        screw_ie8: true
      },
      comments: false,
      sourceMap: true
    })
  )

} else {

// DEVELOPMENT

  config.devtool = 'inline-source-map';

  config.entry = [
    'react-hot-loader/patch',
    // activate HMR for React
    'webpack-dev-server/client?http://localhost:8080',
    // bundle the client for webpack-dev-server
    // and connect to the provided endpoint
    'webpack/hot/only-dev-server',
    // bundle the client for hot reloading
    // only- means to only hot reload for successful updates
    ...config.entry,
  ];

  config.devServer = {
    hot: true,
    // enable HMR on the server
    contentBase: resolve(__dirname, 'dist'),
    // match the output path
    publicPath: '/'
    // match the output `publicPath`
  };

  config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    // enable HMR globally

    new webpack.NamedModulesPlugin()
    // prints more readable module names in the browser console on HMR updates
  )

}

module.exports = config;
