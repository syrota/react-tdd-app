/* eslint-env node, mocha */

// rewrite babel settings to set modules=commonjs

require('babel-register')({
  presets: [
    ['es2015', { modules: 'commonjs' }],
    'stage-2',
    'react'
  ]
});


// ignore css imports

require('ignore-styles')
  // customize extensions:
  // .default(['.css']);


// inject fake 'window', 'navigator' and 'document' into tests

const jsdom = require('jsdom').jsdom;
const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = document.defaultView;

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js'
};