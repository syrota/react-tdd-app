/* eslint-env node, mocha */
module.exports = {
  plugins: [
    require('postcss-cssnext')({ /* ...options */ })
  ]
}