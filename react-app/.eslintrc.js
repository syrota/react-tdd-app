/**
 * @author Eugene Syrota <eugene.syrota@silverlinecrm.com>
 *
 * @see https://medium.com/@dan_abramov/lint-like-it-s-2015-6987d44c5b48#.7lwgznufg
 */

module.exports = {
  parser: 'babel-eslint',
  plugins: [ 'react' ],
  extends: ['eslint:recommended', 'plugin:react/recommended'],

  env: {
    browser: true
  },

  ecmaFeatures: {
    jsx: true,
    modules: true
  },

  globals: {
    fetch: true,
    Promise: true,
    Visualforce: true
  },

  rules: {
    semi: 0,
    'no-unexpected-multiline': 2,
    'no-extra-semi': 2,
    'quotes': [1, 'single'],
    'no-console': [1, { allow: ['warn', 'error'] }]
  }

};
