/* eslint-env node, mocha */

import { expect } from 'chai'
// import { spy } from 'sinon'
// import { mount } from 'enzyme'
import { shallow } from 'enzyme'

import React from 'react'
import App from './App'

describe('<App />', () => {

  it('renders an instance of App', () => {
    expect( shallow(<App />).instance() ).to.be.an.instanceof(App);
  })

})
