import './App.css'
import React, { Component } from 'react'

export default class App extends Component {

  state = {
    index: 0
  }

  onClick = (e) => {
    this.setState({ index: e.target.value })
  }

  render () {
    return (
      <div className="App">
        <h1>This text was rendered by React.js</h1>
        <div>Now, let's test some basic functionality. Button should change it's label after click:</div>
        <div>
        {
          [0,1,2,3].map( index =>
            <button key={ index } value={ index } type="button" onClick={ this.onClick }>
              { index }
              { this.state.index == index && ' active' }
            </button>
          )
        }
        </div>
      </div>
    )
  }

}
