import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App/App'

export function run (rootElement) {
  ReactDOM.render(<App />, rootElement);
}

window.runApp = run;
